#include <atomic>
#include <iostream>

class TicketSpinlock{
    public:
    std::atomic_size_t owner_ticket, next_ticket;

    bool try_lock() {
        std::size_t tmp = owner_ticket;
        return next_ticket.compare_exchange_weak(tmp, tmp + 1);
    }
};

//I think it does what i want it to, albeit i'm not certain it can't be done better.
//The only problem is that if owner is modified after tmp is assigned, we will not be able to lock. But i think it's ok.
