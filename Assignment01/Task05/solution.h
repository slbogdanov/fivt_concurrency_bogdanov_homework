#include <vector>
#include <thread>
#include <iostream>
#include <atomic>

class PetersonMutex{
    std::atomic_bool victim;
    std::atomic_bool resourceRequests[2];
public:
    PetersonMutex(){
        victim = 0;
        resourceRequests[0] = false;
        resourceRequests[1] = false;
    }

    void lock(bool isRight) {
        resourceRequests[isRight] = true;
        victim = isRight;
        while (resourceRequests[!isRight] && victim == isRight) {
            std::this_thread::yield();
        }
    }

    void unlock(bool isRight) {
        resourceRequests[isRight] = false;
    }

    PetersonMutex(const PetersonMutex&) = delete;
    PetersonMutex& operator=(const PetersonMutex&) = delete;
};

class TreeMutex{
    std::vector<PetersonMutex> mutexes;
    std::size_t numberOfThreads;

public:

    TreeMutex(std::size_t numOfThr): numberOfThreads(numOfThr){
        mutexes = std::vector<PetersonMutex>(numberOfThreads + 1);
    }

    void lock(std::size_t current_thread) {
        std::size_t curPos = (numberOfThreads + 1) / 2 + current_thread/2;
        std::size_t prevPos = current_thread;
        while(curPos > 0) {
            mutexes[curPos].lock(prevPos % 2 == 1);
            prevPos = curPos;
            curPos /= 2;
        }
    }

    void unlock(std::size_t current_thread) {
        std::vector<bool> indexesForUnlocking;
        indexesForUnlocking.push_back(current_thread % 2 == 1);
        std::size_t curPos = (numberOfThreads + 1) / 2 + current_thread/2;
        while(curPos > 1) {
            indexesForUnlocking.push_back(curPos % 2 == 1);
            curPos /= 2;
        }
        curPos = 1;
        for(std::size_t i = indexesForUnlocking.size(); i > 0; --i) {
            mutexes[curPos].unlock(indexesForUnlocking[i - 1]);
            curPos = curPos * 2 + indexesForUnlocking[i - 1];
        }
        indexesForUnlocking.clear();
    }

    TreeMutex(const TreeMutex&) = delete;
    TreeMutex& operator=(const TreeMutex&) = delete;
};

